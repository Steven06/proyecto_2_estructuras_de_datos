#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <unistd.h>

#define RESET_COLOR "\x1b[0m"
#define ROJO_T "\x1b[31m"
#define VERDE_T "\x1b[32m"
#define AZUL_T "\x1b[34m"

//Aca se hace el arbol que contendrá la direccion y el alias
struct arbol{
    char direccionURL[400];
    char alias[100];
    struct arbol *izquierda, *derecha;
}arbol;

//Aca se hace el arbol para las direcciones y alias del archivo.
struct arbolArchivo {
    char *URLs[80];
    struct arbolArchivo *siguiente;
}links;

//Aca hacemos estructuras de control para las funciones
struct arbol *raiz = NULL;
struct links *primero = NULL;
struct links *ultimo = NULL;
struct arbolArchivo *siguiente;
int operacion;

//Aca insertamos cada direccion y alias que vamos creando.
void insertar(char *direccionURL, char *alias){

    struct arbol *nuevo = (struct arbol*)malloc(sizeof(arbol));
    strcpy(nuevo->direccionURL, direccionURL);
    strcpy(nuevo->alias, alias);
    nuevo->derecha = NULL;
    nuevo->izquierda = NULL;

    if(raiz==NULL) {
        raiz = nuevo;

    }else {

        struct arbol *anterior, *reco;
        anterior = NULL;
        reco = raiz;
        while (reco != NULL) {

            anterior = reco;
            if (strcmp(direccionURL, reco->direccionURL) < 0) {
                reco = reco->izquierda;
            } else {
                reco = reco->derecha;
            }
        }

        if (strcmp(direccionURL, anterior->direccionURL) < 0) {
            anterior->izquierda = nuevo;

        } else {
            anterior->derecha = nuevo;
        }
    }
}

//funcion que recibe un puntero a un arbol
//y un puntero a caracteres recorre el arbol
//buscando el link deseado no retorna nada

void buscarURL(struct arbol *reconocer, char *aBuscar){

    if(reconocer!=NULL) {
        if (strcmp(reconocer->direccionURL, aBuscar) == 0) {

            char word[220] = "xdg-open ";
            operacion = 1;
            strcat(word, reconocer->direccionURL);
            system(word);

        } else {
            buscarURL(reconocer->izquierda, aBuscar);
            buscarURL(reconocer->derecha, aBuscar);
            operacion = 0;

        }
    }
}


void crearNodoAlias() {
    int seleccion;
    char direccionURL[400];
    char alias[100];
    char enlace[50] = "https://";
    char NoAlias[50] = "No se inserto alias";
    printf("Por favor no ingrese el enlace https://, gracias.");
    printf("Escriba una direccion URL: ");
    scanf("%s", direccionURL);
    strcat(enlace , direccionURL);
    printf("Desea ingresar un alias a su Arbol. 1- Si  2- No");
    scanf("%d", &seleccion);
    if (seleccion == 1) {
        printf("Escriba un alias: ");
        scanf("%s", alias);
        insertar(enlace, alias);
    } else {
        insertar(enlace, NoAlias);
    }
}



//Funcion que recibe un puntero a un arbol
//y no retorna nada, llama a una funcion que busca
//y pregunta si este encontro el link

void buscarURLX(struct arbol *t){

    operacion = 0;
    char aBuscar[200];
    printf("Ingrese el URL a buscar con https://\n");
    scanf("%s", aBuscar);

    buscarURL(t, aBuscar);


    if(operacion == 1){
        printf("Link encontrado con exito\n");
    }else{
        printf("No existe el link \n");
    }
    operacion = 0;
}




//funcion que recibe un puntero a un arbol
//y un puntero a caracteres recorre el arbol
//buscando el alias deseado no retorna nada

void buscarAlias(struct arbol *reco, char *aBuscar){

    if(reco!=NULL) {
        if (strcmp(reco->alias, aBuscar) == 0) {
            char word[220] = "xdg-open ";
            operacion = 1;
            strcat(word, reco->direccionURL);
            system(word);

        } else {
            buscarAlias(reco->izquierda, aBuscar);
            buscarAlias(reco->derecha, aBuscar);
            operacion = 0;
        }
    }
}

//Funcion que recibe un puntero a un arbol
//y no retorna nada, llama a una funcion que busca
//y pregunta si este encontro el alias

void buscarAliasX(struct arbol *t){

    operacion = 0;
    char aBuscar[200];
    printf("Ingrese el alias a buscar");

    scanf("%s", aBuscar);
    buscarAlias(t, aBuscar);

    if(operacion == 1){
        printf("Link encontrado con exito\n");
    }else{
        printf("No existe el link \n");
    }
    operacion = 0;
}

//Funcion que recibe un puntero al arbol y no
//retorna nada, recorre el arbol en preorden
//e imprime

void mostrarLinks(struct arbol *reco){

    if (reco != NULL){
        printf("\n");
        printf("%s", reco->direccionURL);
        printf("\n");
        printf("%s", reco->alias);
        printf("\n");
        mostrarLinks(reco->izquierda);
        mostrarLinks(reco->derecha);

    }
}


//Funcion que recibe un puntero al arbol y un puntero
//a un arreglo de chars no retorna nada, recorre
//el arbol hasta encontrarlo y eliminarlo

void eliminarLink(struct arbol *reco, char *k){

    if(reco!=NULL) {
        if (strcmp(reco->direccionURL, k) == 0) {
            free(reco);
        } else {
            eliminarLink(reco->izquierda, k);
            eliminarLink(reco->derecha, k);
        }
    }
}

//Funcion que recibe un arbol y llama a eliminarlink
//solo captura el link que se desea eliminar no retorna nada

void eliminarPorOpcionLink(struct arbol *eliminardireccion){

    char direccioneliminada[200];
    printf("Digite el link que desea eliminar\n");
    printf("Recuerde ingresar el link con https://\n");
    scanf("%s",direccioneliminada);
    eliminarLink(eliminardireccion, direccioneliminada);
}

//Funcion que recibe un puntero al arbol y un puntero
//a un arreglo de chars no retorna nada, recorre
//el arbol hasta encontrarlo y eliminarlo

void eliminarLinkPorAlias(struct arbol *reco, char *aliasEliminar){

    if(reco!=NULL) {
        if (strcmp(reco->alias, aliasEliminar) == 0) {
            free(reco);
        } else {
            eliminarLink(reco->izquierda, aliasEliminar);
            eliminarLink(reco->derecha, aliasEliminar);
        }
    }
}

//Funcion que recibe un arbol y llama a eliminarlink
//solo captura el alias que se desea eliminar no retorna nada

void eliminarPorOpcionAlias(struct arbol *aliasEliminarOpcion){

    char aliasEliminar[200];
    printf("Digite el alias que desea eliminar\n");
    scanf("%s",aliasEliminar);
    eliminarLinkPorAlias(aliasEliminarOpcion, aliasEliminar);

}

//recorre el string que se le pase leyendo caracter a caracter
//y luego llama a addLista con link creado no retorna nada
//y recibe un arreglo de chars y no retorna nada

void recorrerString(char t[]) {

    char aux = '0';

    int c = 0;
    int j = 0;
    char link[100];

    while (t[c] != ',') {
        aux = t[c];
        c++;
    }

    aux = t[c + 1];


    if (aux == '"') {

        c++;
        c++;
        aux = t[c];

        while (t[c] != '"') {
            if (t[c] =='\0') {
                break;
            } else {
                aux = t[c];
                link[j] = aux;
                j++;
                c++;

            }

        }
        link[j] = '\0';
    }
}

//Funcion de manejo de archivos, no recibe ni retorna nada
//lee el archivo y va llamando a recorrerString hasta leer
//todas las lineas del archivo y luego cierra el flujo

void addTOP() {
    int i;
    int c = 0;
    char aux;
    char temp[200];

    FILE *flujo = fopen("/steven-alvarado-aguilar:~/Descargas/top500Domains.csv", "rb");
    if (flujo == NULL) {
        perror("Error en la apertura del archivo\n");
    }

    while(!feof(flujo)) {
        fgets(temp, 200, flujo);
        recorrerString(temp);
    }
    fclose(flujo);
}




//No recibe ni retorna nada conecta al servidor y descarga el archivo CSV

void descargarTOP(){
    system("xdg-open https://moz.com/top-500/download/?table=top500Domains");
    printf("Por favor espere...\n");
    sleep(10);
    //addTOP();
    printf("Descarga lista!!\n");
    //addArbolTop();

}
//Funcion que recibe un puntero al arbol y un puntero
//a un arreglo de chars no retorna nada, recorre
//el arbol hasta encontrar el alias y luego lo
//actualiza

void actAlias(struct arbol *reco, char *aBuscar ){

    if(reco!=NULL) {
        if (strcmp(reco->direccionURL, aBuscar) == 0) {
            operacion = 1;
            printf("Digite el nuevo alias\n");
            scanf("%s", reco->alias);
            printf("Actualizado correctamente\n");
        } else {
            actAlias(reco->izquierda, aBuscar);
            actAlias(reco->derecha, aBuscar);
            operacion = 0;
        }

    }

}
//Funcion que recibe un puntero a un arbol
//y no retorna nada, llama a una funcion que busca
//y pregunta si este es el alias luego lo actualiza

void actualizarAlias(struct arbol *t) {
    operacion = 0;
    char aBuscar[200];
    printf("Recuerde ingresar el link con https://\n");
    printf("Ingrese el URL al que desea cambiar alias\n");
    scanf("%s", aBuscar);

    actAlias(t, aBuscar);

    if(operacion == 1){
        printf("Alias cambiado con exito\n");

    }else{
        printf("No existe el link \n");
    }
    operacion = 0;
}



void imprimirOpciones() {
    printf(AZUL_T"\n---------------------------------------------------------------------"RESET_COLOR);
    printf(AZUL_T"\n(1) Crear nueva URL y alias."RESET_COLOR);
    printf(AZUL_T"\n(2) Buscar la dirección."RESET_COLOR);
    printf(AZUL_T"\n(3) Buscar el alias."RESET_COLOR);
    printf(ROJO_T"\n(4) Mostrar los links."RESET_COLOR);
    printf(ROJO_T"\n(5) Descargar el archivo CSV."RESET_COLOR);
    printf(ROJO_T"\n(6) Eliminar por URL"RESET_COLOR);
    printf(AZUL_T"\n(7) Eliminar por Alias"RESET_COLOR);
    printf(AZUL_T"\n(8) Actualizar Alias"RESET_COLOR);
    printf(VERDE_T"\n(9) Salir"RESET_COLOR);
    printf(VERDE_T"\n---------------------------------------------------------------------");
    printf(VERDE_T"\n>> Cuál es la opción deseada: "RESET_COLOR);

    int opcion;
    scanf("%d", &opcion);
    switch (opcion) {
        case 1:
            crearNodoAlias();
            imprimirOpciones();
            break;
        case 2:
            buscarURLX(raiz);
            imprimirOpciones();
            break;
        case 3:
            buscarAliasX(raiz);
            imprimirOpciones();
            break;
        case 4:
            mostrarLinks(raiz);
            imprimirOpciones();
            break;
        case 5:
            descargarTOP();
            imprimirOpciones();
            break;
        case 6:
            eliminarPorOpcionLink(raiz);
            imprimirOpciones();
            break;
        case 7:
            eliminarPorOpcionAlias(raiz);
            imprimirOpciones();
            break;
        case 8:
            actualizarAlias(raiz);
            imprimirOpciones();
            break;
        case 9:
            default:
                break;
    }
}

int main() {
    imprimirOpciones();
}