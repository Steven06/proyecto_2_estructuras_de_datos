#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define RESET_COLOR "\x1b[0m"
#define ROJO_T "\x1b[31m"
#define VERDE_T "\x1b[32m"
#define AZUL_T "\x1b[34m"

struct nodo {
    char URLs[80];
    struct nodo *izquierda, *derecha;
};

struct nodoAlias{
    char *direccionURL;
    char *alias;
    struct nodoAlias *izquierda, *derecha;
};

void imprimirOpciones() {
    printf(AZUL_T"\n---------------------------------------------------------------------"RESET_COLOR);
    printf(AZUL_T"\n(1) Crear nueva URL y alias."RESET_COLOR);
    printf(AZUL_T"\n(2) Insertar nueva URLy alias."RESET_COLOR);
    printf(AZUL_T"\n(3) Imprimir el árbol de urls y alias."RESET_COLOR);
    printf(ROJO_T"\n(4) Descargar el archivo CSV."RESET_COLOR);
    printf(ROJO_T"\n(5) Imprimir el árbol de urls del archivo."RESET_COLOR);
    printf(ROJO_T"\n(6) Buscar por URL"RESET_COLOR);
    printf(AZUL_T"\n(7) Buscar por Alias"RESET_COLOR);
    printf(AZUL_T"\n(8) Actualizar Alias"RESET_COLOR);
    printf(VERDE_T"\n(9) Borrar por URL"RESET_COLOR);
    printf(VERDE_T"\n(10) Salir"RESET_COLOR);
    printf(VERDE_T"\n---------------------------------------------------------------------");
    printf(VERDE_T"\n>> Cuál es la opción deseada: "RESET_COLOR);
}

struct nodo *raiz = NULL;

struct nodoAlias* crearNodoAlias(char *direccionURL, char *alias){
    struct nodoAlias *nodo = (struct nodoAlias*)malloc(sizeof(struct nodoAlias));
    nodo->direccionURL = direccionURL;
    nodo->alias = alias;
    nodo->izquierda = nodo->derecha = NULL;
    return (nodo);
}

void insertar(char *direccionURL, char *alias, struct nodoAlias *raiz) {
    struct nodoAlias *nuevo = crearNodoAlias(direccionURL, alias);

    if (raiz == NULL) {
        raiz = nuevo;
    }
    if(nuevo->direccionURL, nuevo->alias >= raiz->direccionURL, raiz->alias){
        if(raiz->derecha == NULL){
            raiz->derecha = nuevo;
        }else{
            insertar(direccionURL, alias, raiz->derecha);
        }
    }else{
        if (raiz->izquierda == NULL){
            raiz->izquierda = nuevo;
        } else{
            insertar(direccionURL, alias, raiz->izquierda);
        }
    }
}

void imprimir(struct nodoAlias *raiz){
    if(raiz != NULL){
        imprimir(raiz->izquierda);
        printf("%s %s", raiz->direccionURL, raiz->alias);
        imprimir(raiz->derecha);
    }
}

struct nodo* crearNodoURLLista(char URLs[80]){
    struct nodo *nodo = (struct nodo*)malloc(sizeof(struct nodo));
    strcpy(nodo->URLs, URLs);
    nodo->izquierda = nodo->derecha = NULL;
    return (nodo);
}

void imprimirArbolLista(struct nodo *raizLista);

void insertarListaURLs(char URLs[80], struct nodo *raizLista){
    struct nodo *nuevo = malloc(sizeof(struct nodo));
    nuevo->URLs, URLs;
    nuevo->derecha = NULL;
    nuevo->izquierda = NULL;

    if(raizLista == NULL){
        raizLista = nuevo;
    }
    else{
        struct nodo *anterior, *nodoareconocer;
        anterior = NULL;
        nodoareconocer = raizLista;

        while(nodoareconocer != NULL){
            anterior = nodoareconocer;
            if(URLs < nodoareconocer->URLs){
                nodoareconocer = nodoareconocer->izquierda;
            }
            else{
                nodoareconocer = nodoareconocer->derecha;
            }
            if(URLs < anterior->URLs){
                anterior->izquierda = nuevo;
            }
            else{
                anterior->derecha = nuevo;
            }
        }
    }
}

void imprimirArbolLista(struct nodo *raizLista) {
    if(raizLista != NULL){
        imprimirArbolLista(raizLista->izquierda);
        printf("%s", raizLista->URLs);
        imprimirArbolLista(raizLista->derecha);
    }
}

int buscarPorURL(struct nodo *arbol, char URLs[80]){

    int resultado = 0;  //0 indica que ya encontre el nodo búscado

    if(arbol == NULL){
        return resultado;
    }

    if(URLs < arbol->URLs){
        resultado = buscarPorURL(arbol->izquierda, URLs);
    }

    if(URLs > arbol->URLs){
        resultado = buscarPorURL(arbol->derecha, URLs);
    }

    else{
        resultado = 1;  // son iguales, lo encontre
    }

    return resultado;

}

int buscarPorAlias(struct nodoAlias *arbolAlias, char *alias){

    int resultado = 0;  //0 indica que ya encontre el nodo búscado

    if(arbolAlias == NULL){
        return resultado;
    }

    if(alias < arbolAlias->alias){
        resultado = buscarPorAlias(arbolAlias->izquierda, alias);
    }

    if(alias > arbolAlias->alias){
        resultado = buscarPorAlias(arbolAlias->derecha, alias);
    }

    else{
        resultado = 1;  // son iguales, lo encontre
    }

    return resultado;

}

void actualizarAlias(struct nodoAlias *arbol, char *alias){

}

void borrarPorURL(struct nodo *direccionURL){
    if(direccionURL != NULL){
        borrarPorURL(direccionURL->izquierda);
        borrarPorURL(direccionURL->derecha);
        free(direccionURL);
    }
}

void borrarPorAlias(struct nodo *alias){
    if(alias != NULL){
        borrarPorAlias(alias->izquierda);
        borrarPorAlias(alias->derecha);
        free(alias);
    }
}

char convirtirElArchivo(char URLsModificados[80]){
    int ultimoCaracter = strlen(URLsModificados);
    ultimoCaracter--;
    URLsModificados[ultimoCaracter] = '\000';
    ultimoCaracter--;
    URLsModificados[ultimoCaracter] = '\000';
    for(int indice = 0; indice < ultimoCaracter + 1; indice++){
        URLsModificados[indice] = URLsModificados[indice + 1];
    }
    return URLsModificados[80];
}

int abrirArchivo(){
    char *botonDeDescarga;
    char *separarcolumna;
    char *obtenerColumna2;
    char *eliminar500dominios;
    struct nodo *arbol;

    system("'botonDeDescarga' wget -O 'Documentos/II_Semestre/Estructuras_de_datos/Proyecto_2' https://moz.com/top-500/download/?table=top500Domains");
    system("'separarcolumna' columna -s, -t <top500Domains.csv>> 500dominios.txt");
    system("'obtenerColumna2' awk '{print $2}' 500dominios.txt>> top500URL.txt");
    system("'eliminar500dominios' rm 500dominios.txt");


    FILE *top500URL;
    top500URL = fopen("top500URL.txt", "r");

    char URLs[80];

    while (!feof(top500URL)){
        fgets(URLs, 80, top500URL);
        if (strcmp(URLs, "\"raiz\n") != 0){
            URLs[80] = convirtirElArchivo(URLs);
            insertarListaURLs(URLs, arbol);
        }
    }
    fclose(top500URL) ; // closing the file
    printf("Cerrando el archivo top500Domains.csv") ;
}

int main() {
    char direccionURL;
    char alias;
    struct nodo *arbol;
    struct nodoAlias *arbolAlias;
    int opcion;
    while(opcion){
        imprimirOpciones();
        scanf("%d", &opcion);
        switch (opcion) {
            case 1:
                printf("Escriba una direccion URL: ");
                scanf("%s", &direccionURL);
                printf("Escriba un alias (Sí no escriba NULL.): ");
                scanf("%s", &alias);
               struct nodoAlias *arbolAlias = crearNodoAlias(&direccionURL, &alias);
                main();
                break;
            case 2:
                printf("Escriba una direccion URL: ");
                scanf("%s", &direccionURL);
                printf("Escriba un alias (Sí no escriba NULL.): ");
                scanf("%s", &alias);
                insertar(&direccionURL, &alias, arbolAlias);
                main();
                break;
            case 3:
                imprimir(arbolAlias);
                main();
                break;
            case 4:
                abrirArchivo();
                main();
                break;
            case 5:
                imprimirArbolLista(arbol);
                main();
                break;
            case 6:
                printf("Escriba un direccion URL: ");
                scanf("%s", &direccionURL);
                buscarPorURL(arbol, &direccionURL);
                main();
                break;
            case 7:
                printf("Escriba un alias: ");
                scanf("%s", &alias);
                buscarPorAlias(arbolAlias, &alias);
                main();
                break;
            case 8:
                printf("Escriba un alias: ");
                scanf("%s", &alias);
                actualizarAlias(arbolAlias, &alias);
                main();
                break;
            case 9:
                printf("Escriba un direccion URL: ");
                scanf("%s", &direccionURL);
                borrarPorURL(raiz);
                main();
                break;
            case 10:
                exit(-2);
            default:
                main();
                return 0;
        }
    }
}